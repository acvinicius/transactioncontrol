package com.bank.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;

/**
 * The Class Account.
 * 
 * @author viniciuscastro
 */
@Entity
@Table(name = "TBACCOUNT")
public class Account implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4410748369124157424L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, name = "account_id")
    private Long id;

	/** The user name. */
	@Column(name = "userName", unique = true)
    private String userName;

	/** The balance. */
	@Column(name = "balance")
    private Float balance;

	/** The transaction history. */
	@OneToMany(targetEntity = AccountTransaction.class, mappedBy="account_id", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	@ForeignKey(name = "FK_ACCOUNT_ACCOUNTTRANSACTION")
    private List<AccountTransaction> transactionHistory;

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the transaction history.
     *
     * @param transactionHistory the new transaction history
     */
    public void setTransactionHistory(List<AccountTransaction> transactionHistory) {
        this.transactionHistory = transactionHistory;
    }

    /**
     * Gets the transaction history.
     *
     * @return the transaction history
     */
    public List<AccountTransaction> getTransactionHistory() {
        return transactionHistory;
    }

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public Float getBalance() {
		return balance;
	}

	/**
	 * Sets the balance.
	 *
	 * @param balance the new balance
	 */
	public void setBalance(Float balance) {
		this.balance = balance;
	}

}
