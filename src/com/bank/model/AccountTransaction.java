package com.bank.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Class AccountTransaction.
 * 
 * @author viniciuscastro
 */
@Entity
@Table(name = "TBACCOUNTTRANSACTION")
public class AccountTransaction implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2084810017016625766L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "transaction_id")
	private Long id;

	/** The account_id. */
	@ManyToOne(targetEntity = Account.class, fetch=FetchType.EAGER)
	private Account account_id;

	/** The transaction type. */
	@Column(name = "transactionType")
	private String transactionType;

	/** The value. */
	@Column(name = "value")
	private Float value;

	/** The balance. */
	@Column(name = "balance")
	private Float balance;

	/** The changed date. */
	@Column(name = "changedDate")
	private Date changedDate;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the account_id.
	 *
	 * @return the account_id
	 */
	public Account getAccount_id() {
		return account_id;
	}

	/**
	 * Sets the account_id.
	 *
	 * @param account_id the new account_id
	 */
	public void setAccount_id(Account account_id) {
		this.account_id = account_id;
	}

	/**
	 * Gets the transaction type.
	 *
	 * @return the transaction type
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * Sets the transaction type.
	 *
	 * @param transactionType the new transaction type
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public Float getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(Float value) {
		this.value = value;
	}

	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public Float getBalance() {
		return balance;
	}

	/**
	 * Sets the balance.
	 *
	 * @param balance the new balance
	 */
	public void setBalance(Float balance) {
		this.balance = balance;
	}

	/**
	 * Gets the changed date.
	 *
	 * @return the changed date
	 */
	public Date getChangedDate() {
		return changedDate;
	}

	/**
	 * Sets the changed date.
	 *
	 * @param changedDate the new changed date
	 */
	public void setChangedDate(Date changedDate) {
		this.changedDate = changedDate;
	}


}
