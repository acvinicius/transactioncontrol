package com.bank.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.bank.dao.AccountDAO;
import com.bank.model.Account;
import com.bank.service.IAccount;

/**
 * The Class AccountImpl.
 * 
 * @author viniciuscastro
 */
public class AccountImpl implements IAccount {

	/** The dao. */
	private AccountDAO dao = new AccountDAO();
	
	/**
	 * Instantiates a new account impl.
	 */
	public AccountImpl() {
		
	}
	
	/* (non-Javadoc)
	 * @see com.bank.service.IAccount#createAccount(com.bank.model.Account)
	 */
	public void createAccount(Account account) {
		try {
			this.dao.createAccount(account);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.bank.service.IAccount#removeAccount(com.bank.model.Account)
	 */
	public void removeAccount(Account account) {
		try {
			this.dao.removeAccount(account);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.bank.service.IAccount#listOfAccounts()
	 */
	public List<Account> listOfAccounts() {
		return this.dao.listOfAccounts();
	}
	
	/**
	 * Find account.
	 *
	 * @param selectAccount the select account
	 * @return the account
	 */
	public Account findAccount(Account selectAccount) {
		return this.dao.findAccount(selectAccount);
	}
	
}
