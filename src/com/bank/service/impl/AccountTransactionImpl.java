package com.bank.service.impl;

import java.sql.SQLException;

import com.bank.dao.AccountTransactionDAO;
import com.bank.model.Account;
import com.bank.service.IAccountTransaction;

/**
 * The Class AccountTransactionImpl.
 * 
 * @author viniciuscastro
 */
public class AccountTransactionImpl implements IAccountTransaction {

	/** The dao. */
	private AccountTransactionDAO dao = new AccountTransactionDAO();
	
	/**
	 * Instantiates a new account transaction impl.
	 */
	public AccountTransactionImpl() {
		
	}
	
	/* (non-Javadoc)
	 * @see com.bank.service.IAccountTransaction#deposit(com.bank.model.Account)
	 */
	public void deposit(Account accountTransaction) {
		try {
			this.dao.updateTransaction(accountTransaction);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.bank.service.IAccountTransaction#withdrawValue(com.bank.model.Account)
	 */
	public void withdrawValue(Account accountTransaction) {
		try {
			this.dao.updateTransaction(accountTransaction);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.bank.service.IAccountTransaction#transferValue(com.bank.model.Account, com.bank.model.Account)
	 */
	public void transferValue(Account accountTransactionSending,
			Account accountTransactionReceiving) {
		try {
			this.dao.updateTransaction(accountTransactionReceiving);
			this.dao.updateTransaction(accountTransactionSending);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
