package com.bank.service;

import com.bank.model.Account;

/**
 * The Interface IAccountTransaction.
 * 
 * @author viniciuscastro
 */
public interface IAccountTransaction {

	/**
	 * Deposit.
	 *
	 * @param accountTransaction the account transaction
	 */
	public void deposit(Account accountTransaction);
	
	/**
	 * Withdraw value.
	 *
	 * @param accountTransaction the account transaction
	 */
	public void withdrawValue(Account accountTransaction);
	
	/**
	 * Transfer value.
	 *
	 * @param accountTransactionSending the account transaction sending
	 * @param accountTransactionReceiving the account transaction receiving
	 */
	public void transferValue(Account accountTransactionSending, Account accountTransactionReceiving);
	
}
