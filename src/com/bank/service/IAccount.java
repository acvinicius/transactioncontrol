package com.bank.service;

import java.util.List;

import com.bank.model.Account;

/**
 * The Interface IAccount.
 * 
 * @author viniciuscastro
 */
public interface IAccount {

	/**
	 * Creates the account.
	 *
	 * @param account the account
	 */
	public void createAccount(Account account);
	
	/**
	 * Removes the account.
	 *
	 * @param account the account
	 */
	public void removeAccount(Account account);
	
	/**
	 * List of accounts.
	 *
	 * @return the list
	 */
	public List<Account> listOfAccounts();
	
}
