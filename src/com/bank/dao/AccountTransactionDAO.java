package com.bank.dao;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.ejb.HibernatePersistence;

import com.bank.model.Account;
import com.bank.util.HibernateUtil;

/**
 * The Class AccountTransactionDAO.
 * 
 * @author viniciuscastro
 */
public class AccountTransactionDAO extends HibernatePersistence {
	
	/**
	 * Instantiates a new account transaction dao.
	 */
	public AccountTransactionDAO() {
		
	}
	
	/**
	 * Update transaction.
	 *
	 * @param accountTransaction the account transaction
	 * @throws SQLException the SQL exception
	 */
	public void updateTransaction(Account accountTransaction) throws SQLException {
		try {
			SessionFactory sf = HibernateUtil.getSessionFactory();
			Session session = sf.openSession();
			session.beginTransaction();
			session.update(accountTransaction);
			session.getTransaction().commit();
			session.close();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}

}
