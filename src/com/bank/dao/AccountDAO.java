package com.bank.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.ejb.HibernatePersistence;

import com.bank.model.Account;
import com.bank.util.HibernateUtil;

/**
 * The Class AccountDAO.
 * 
 * @author viniciuscastro
 */
public class AccountDAO extends HibernatePersistence {
	
	/**
	 * Instantiates a new account dao.
	 */
	public AccountDAO() {
		
	}

	/**
	 * Creates the account.
	 *
	 * @param account the account
	 * @throws SQLException the SQL exception
	 */
	public void createAccount(Account account) throws SQLException {
		try {
			SessionFactory sf = HibernateUtil.getSessionFactory();
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(account);
			session.getTransaction().commit();
			session.close();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Removes the account.
	 *
	 * @param account the account
	 * @throws SQLException the SQL exception
	 */
	public void removeAccount(Account account) throws SQLException {
		try {
			SessionFactory sf = HibernateUtil.getSessionFactory();
			Session session = sf.openSession();
			session.beginTransaction();
			session.delete(account);
			session.getTransaction().commit();
			session.close();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}

	/**
	 * List of accounts.
	 *
	 * @return the list
	 */
	public List<Account> listOfAccounts() {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		Query q = session.createQuery("SELECT a FROM Account a");
		@SuppressWarnings("unchecked")
		List<Account> accountsList = q.list();
		session.getTransaction().commit();
		session.close();
		return accountsList;
	}
	
	/**
	 * Find account.
	 *
	 * @param selectAccount the select account
	 * @return the account
	 */
	public Account findAccount (Account selectAccount) {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		Query q = session.createQuery("SELECT a FROM Account a Where a.id = " + selectAccount.getId());
		Account accountResult = (Account) q.uniqueResult();
		session.getTransaction().commit();
		session.close();
		return accountResult;
	}
}
