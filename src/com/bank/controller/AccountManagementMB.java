package com.bank.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.bank.model.Account;
import com.bank.model.AccountTransaction;
import com.bank.service.impl.AccountImpl;

/**
 * The Class AccountManagementMB.
 * 
 * @author viniciuscastro
 */
@RequestScoped
@ManagedBean
public class AccountManagementMB implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3052600141475240652L;

	/** The new account. */
	private Account newAccount = new Account();

	/** The account. */
	private Account account = new Account();
	
	/** The accounts list. */
	private List<Account> accountsList = new ArrayList<Account>();
	
	/** The account impl. */
	private AccountImpl accountImpl = new AccountImpl();

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		accountsList = accountImpl.listOfAccounts();
	}
	
	/**
	 * Instantiates a new account management mb.
	 */
	public AccountManagementMB() {
		accountsList = accountImpl.listOfAccounts();
	}

	/**
	 * Selected account.
	 *
	 * @param accountId the account id
	 */
	public void selectedAccount(Account accountId) {
		account.setId(accountId.getId());
	}
	
	/**
	 * Gets the new account.
	 *
	 * @return the new account
	 */
	public Account getNewAccount() {
		return newAccount;
	}

	/**
	 * Sets the new account.
	 *
	 * @param newAccount the new new account
	 */
	public void setNewAccount(Account newAccount) {
		this.newAccount = newAccount;
	}

	/**
	 * Creates the account.
	 *
	 * @return the string
	 */
	public String createAccount() {
		if (newAccount.getUserName() == null || newAccount.getUserName().isEmpty() || newAccount.getBalance() == null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Please, complete all form before submit."));
			return null;
		}
		else {
			AccountTransaction firstTransaction = new AccountTransaction();
			firstTransaction.setChangedDate(Calendar.getInstance().getTime());
			firstTransaction.setBalance(newAccount.getBalance());
			firstTransaction.setValue(newAccount.getBalance());
			firstTransaction.setTransactionType("Deposit");
			firstTransaction.setAccount_id(newAccount);
			newAccount.setTransactionHistory(new ArrayList<AccountTransaction>());
			newAccount.getTransactionHistory().add(firstTransaction);
			accountImpl.createAccount(newAccount);
			return "accountManagement";
		}
	}
	
	/**
	 * Removes the account.
	 *
	 * @param accountId the account id
	 * @return the string
	 */
	public String removeAccount(Long accountId) {
		account.setId(accountId);
		account = accountImpl.findAccount(account);
		if (account != null && account.getId() != null) {
			accountImpl.removeAccount(account);
		}
		return "accountManagement";
	}

	/**
	 * Open account.
	 *
	 * @return the string
	 */
	public String openAccount() {
		if (account != null && account.getId() != null) {
			return "account";
		}
		return "";
	}
	
	/**
	 * Gets the accounts list.
	 *
	 * @return the accounts list
	 */
	public List<Account> getAccountsList() {
		return accountsList;
	}

	/**
	 * Sets the accounts list.
	 *
	 * @param accountsList the new accounts list
	 */
	public void setAccountsList(List<Account> accountsList) {
		this.accountsList = accountsList;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}
}
