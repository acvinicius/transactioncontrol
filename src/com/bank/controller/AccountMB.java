package com.bank.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.bank.model.Account;
import com.bank.model.AccountTransaction;
import com.bank.service.impl.AccountImpl;

/**
 * The Class AccountMB.
 * 
 * @author viniciuscastro
 */
@RequestScoped
@ManagedBean
public class AccountMB implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7678753911955695623L;

	/** The Constant PARAMETER_KEY_ACCOUNT. */
	private static final String PARAMETER_KEY_ACCOUNT = "accountKey"; 
	
	/** The account transactions. */
	private List<AccountTransaction> accountTransactions = new ArrayList<AccountTransaction>();
	
	/** The current account. */
	private Account currentAccount = new Account();
	
	/** The account impl. */
	private AccountImpl accountImpl = new AccountImpl();
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){}
	
	/**
	 * Instantiates a new account mb.
	 */
	public AccountMB() {
		Account a = new Account();
		HttpServletRequest request = (HttpServletRequest)         
				FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String s = request.getParameter(PARAMETER_KEY_ACCOUNT);
		if (s != null && !s.equals("")) {
			a.setId(Long.parseLong(s));
		}
		if (a.getId() != null) {
			currentAccount = accountImpl.findAccount(a);
		}
	}
	
	/**
	 * Perform transaction.
	 *
	 * @return the string
	 */
	public String performTransaction(){
		return "performTransaction";
	}

	/**
	 * Gets the account transactions.
	 *
	 * @return the account transactions
	 */
	public List<AccountTransaction> getAccountTransactions() {
		return accountTransactions;
	}

	/**
	 * Sets the account transactions.
	 *
	 * @param accountTransactions the new account transactions
	 */
	public void setAccountTransactions(List<AccountTransaction> accountTransactions) {
		this.accountTransactions = accountTransactions;
	}

	/**
	 * Gets the current account.
	 *
	 * @return the current account
	 */
	public Account getCurrentAccount() {
		return currentAccount;
	}

	/**
	 * Sets the current account.
	 *
	 * @param currentAccount the new current account
	 */
	public void setCurrentAccount(Account currentAccount) {
		this.currentAccount = currentAccount;
	}
	
	
}
