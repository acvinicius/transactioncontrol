package com.bank.controller;

import java.io.Serializable;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.bank.model.Account;
import com.bank.model.AccountTransaction;
import com.bank.service.impl.AccountImpl;
import com.bank.service.impl.AccountTransactionImpl;

/**
 * The Class AccountTransactionMB.
 * 
 * @author viniciuscastro
 */
@RequestScoped
@ManagedBean
public class AccountTransactionMB implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2827374271245323291L;

	/** The account transaction. */
	private AccountTransaction accountTransaction = new AccountTransaction();
	
	/** The account. */
	private Account account = new Account();

	/** The account receiving. */
	private Account accountReceiving = new Account();
	
	/** The transaction type. */
	private String transactionType = "";

	/** The transaction type transfer. */
	private boolean transactionTypeTransfer = false;
	
	/** The Constant PARAMETER_KEY_ACCOUNT. */
	private static final String PARAMETER_KEY_ACCOUNT = "accountKey"; 

	/** The account impl. */
	private AccountImpl accountImpl = new AccountImpl();
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){}
	
	/** The account transaction impl. */
	private AccountTransactionImpl accountTransactionImpl = new AccountTransactionImpl(); 
	
	/**
	 * Instantiates a new account transaction mb.
	 */
	public AccountTransactionMB() {
		Account a = new Account();
		HttpServletRequest request = (HttpServletRequest)         
				FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String s = request.getParameter(PARAMETER_KEY_ACCOUNT);
		if (s != null && !s.equals("")) {
			a.setId(Long.parseLong(s));
		}
		if (a.getId() != null) {
			account = accountImpl.findAccount(a);
		}
	}
	
	/**
	 * Execute transaction.
	 *
	 * @return the string
	 */
	public String executeTransaction () {
		accountTransaction.setTransactionType(transactionType.toString());
		boolean transactionReturn = Boolean.FALSE;
		if (accountTransaction.getTransactionType().equals("Deposit")) {
			transactionReturn = deposit(account, accountTransaction);
		}
		else if (accountTransaction.getTransactionType().equals("Withdraw")) {
			transactionReturn = withdraw(account, accountTransaction);
		}		
		else if (accountTransaction.getTransactionType().equals("Transfer")) {
			transactionReturn = transfer(account, accountTransaction, accountReceiving);
		}
		if (transactionReturn) {
			return "confirm";
		}
		else {
			return null;
		}
	}
	
	
	/**
	 * Deposit.
	 *
	 * @param account the account
	 * @param accountTransaction the account transaction
	 * @return true, if successful
	 */
	public boolean deposit(Account account, AccountTransaction accountTransaction) {
		account.setBalance(account.getBalance() + accountTransaction.getValue());
		accountTransaction.setBalance(account.getBalance());
		accountTransaction.setChangedDate(Calendar.getInstance().getTime());
		accountTransaction.setAccount_id(account);
		account.getTransactionHistory().add(accountTransaction);
		accountTransactionImpl.deposit(account);
		return Boolean.TRUE;
    }

    /**
     * Withdraw.
     *
     * @param account the account
     * @param accountTransaction the account transaction
     * @return true, if successful
     */
    public boolean withdraw(Account account, AccountTransaction accountTransaction) {
        if (accountTransaction.getValue() > account.getBalance()) {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("You do not have enough money in your account to do this transaction."));
			return Boolean.FALSE;
        }
        else {
        	account.setBalance(account.getBalance() - accountTransaction.getValue());
        	accountTransaction.setBalance(account.getBalance());
        	accountTransaction.setChangedDate(Calendar.getInstance().getTime());
        	accountTransaction.setAccount_id(account);
        	account.getTransactionHistory().add(accountTransaction);
        	accountTransactionImpl.withdrawValue(account);
        	return Boolean.TRUE;
        }
    }
    
    /**
     * Transfer.
     *
     * @param account the account
     * @param accountTransaction the account transaction
     * @param accountReceiving the account receiving
     * @return true, if successful
     */
    public boolean transfer(Account account, AccountTransaction accountTransaction, Account accountReceiving) {
    	if (accountTransaction.getValue() > account.getBalance()) {
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("You do not have enough money in your account to do this transaction."));
    		return Boolean.FALSE;
        }
    	else {
    		//Account Receiving
    		accountReceiving = accountImpl.findAccount(accountReceiving);
    		if (accountReceiving == null) {
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Please, insert a valid receiving account."));
    			return Boolean.FALSE;
    		}
    		accountReceiving.setBalance(accountReceiving.getBalance() + accountTransaction.getValue());
    		AccountTransaction accountTransactionReceiving = new AccountTransaction();
    		accountTransactionReceiving.setBalance(accountReceiving.getBalance());
    		accountTransactionReceiving.setChangedDate(Calendar.getInstance().getTime());
    		accountTransactionReceiving.setAccount_id(accountReceiving);
    		accountTransactionReceiving.setTransactionType(accountTransaction.getTransactionType());
    		accountTransactionReceiving.setValue(accountTransaction.getValue());
    		accountReceiving.getTransactionHistory().add(accountTransactionReceiving);
    		//Account withdraw
    		account.setBalance(account.getBalance() - accountTransaction.getValue());
    		accountTransaction.setBalance(account.getBalance());
    		accountTransaction.setChangedDate(Calendar.getInstance().getTime());
    		accountTransaction.setAccount_id(account);
    		account.getTransactionHistory().add(accountTransaction);
    		//Transfer Transaction
    		accountTransactionImpl.transferValue(account, accountReceiving);
    		return Boolean.TRUE;
    	}
    }

    /**
     * Gets the transaction type.
     *
     * @return the transaction type
     */
    public String getTransactionType() {
    	return transactionType;
    }
    
    /**
     * Sets the transaction type.
     *
     * @param transactionType the new transaction type
     */
    public void setTransactionType(String transactionType) {
    	this.transactionType = transactionType;
    }

	/**
	 * Gets the account transaction.
	 *
	 * @return the account transaction
	 */
	public AccountTransaction getAccountTransaction() {
		return accountTransaction;
	}

	/**
	 * Sets the account transaction.
	 *
	 * @param accountTransaction the new account transaction
	 */
	public void setAccountTransaction(AccountTransaction accountTransaction) {
		this.accountTransaction = accountTransaction;
	}

	/**
	 * Gets the account.
	 *
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Sets the account.
	 *
	 * @param account the new account
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * Gets the account receiving.
	 *
	 * @return the account receiving
	 */
	public Account getAccountReceiving() {
		return accountReceiving;
	}

	/**
	 * Sets the account receiving.
	 *
	 * @param accountReceiving the new account receiving
	 */
	public void setAccountReceiving(Account accountReceiving) {
		this.accountReceiving = accountReceiving;
	}

	/**
	 * Checks if is transaction type transfer.
	 *
	 * @return true, if is transaction type transfer
	 */
	public boolean isTransactionTypeTransfer() {
		if (transactionType != null && transactionType.equals("Transfer")) {
			transactionTypeTransfer = Boolean.TRUE;
		}
		return transactionTypeTransfer;
	}

	/**
	 * Sets the transaction type transfer.
	 *
	 * @param transactionTypeTransfer the new transaction type transfer
	 */
	public void setTransactionTypeTransfer(boolean transactionTypeTransfer) {
		this.transactionTypeTransfer = transactionTypeTransfer;
	}
}
