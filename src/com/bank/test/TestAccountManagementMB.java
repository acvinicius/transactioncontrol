package com.bank.test;

import java.util.ArrayList;
import java.util.Calendar;

import com.bank.model.Account;
import com.bank.model.AccountTransaction;
import com.bank.service.impl.AccountImpl;

import junit.framework.TestCase;

/**
 * The Class TestAccountManagementMB.
 * 
 * @author viniciuscastro
 */
public class TestAccountManagementMB extends TestCase {

	/**
	 * Instantiates a new test account management mb.
	 *
	 * @param name the name
	 */
	public TestAccountManagementMB(String name) {
		super(name);
	}

	/** The account impl. */
	private AccountImpl accountImpl;
	
	/** The account. */
	private Account account;
	
	/** The account test. */
	private Account accountTest;
	
	/** The transaction history. */
	private AccountTransaction transactionHistory;
	
	/** The index. */
	private Integer index;

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		accountImpl = new AccountImpl();
		account = new Account();
		accountTest = null;
		transactionHistory = new AccountTransaction();
		index = null;
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test create account.
	 */
	public final void testCreateAccount() {
		account.setBalance(1000.00f);
		account.setUserName("Test" + Math.random());
		
		transactionHistory.setTransactionType("Deposit");
		transactionHistory.setChangedDate(Calendar.getInstance().getTime());
		transactionHistory.setValue(1000.00f);
		transactionHistory.setBalance(1000.00f);
		transactionHistory.setAccount_id(account);
		account.setTransactionHistory(new ArrayList<AccountTransaction>());
		account.getTransactionHistory().add(transactionHistory);
		
		accountImpl.createAccount(account);
		accountTest = accountImpl.findAccount(account);
		assertEquals(account.getUserName(), accountTest.getUserName());
		assertEquals(account.getBalance(), accountTest.getBalance());
		assertEquals(account.getId(), accountTest.getId());
		if (account.getTransactionHistory() != null) {
			index = account.getTransactionHistory().size() - 1;
			if (index != null && index >= 0) {
				assertEquals(account.getTransactionHistory().get(index).getTransactionType(), accountTest.getTransactionHistory().get(index).getTransactionType());
				assertEquals(account.getTransactionHistory().get(index).getAccount_id().getId(), accountTest.getTransactionHistory().get(index).getAccount_id().getId());
				assertEquals(account.getTransactionHistory().get(index).getBalance(), accountTest.getTransactionHistory().get(index).getBalance());
				assertEquals(account.getTransactionHistory().get(index).getId(), accountTest.getTransactionHistory().get(index).getId());
				assertEquals(account.getTransactionHistory().get(index).getValue(), accountTest.getTransactionHistory().get(index).getValue());
			}
			
		}
	}

	/**
	 * Test remove account.
	 */
	public final void testRemoveAccount() {
		//Set the accountId that you will delete.
		account.setId(5L);
		
		account = accountImpl.findAccount(account);
		if (account != null) {
			accountImpl.removeAccount(account);
		}
		accountTest = accountImpl.findAccount(account);
		assertNull(accountTest);
	}

}
