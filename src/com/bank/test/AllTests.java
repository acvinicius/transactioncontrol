package com.bank.test;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(TestAccountManagementMB.class);
		suite.addTestSuite(TestAccountTransactionMB.class);
//		suite.addTestSuite(TestAccountCreate.class);
//		suite.addTestSuite(TestAccountDelete.class);
//		suite.addTestSuite(TestAccountDeposit.class);
//		suite.addTestSuite(TestAccountTransfer.class);
//		suite.addTestSuite(TestAccountWithdraw.class);
		//$JUnit-END$
		return suite;
	}

}
