package com.bank.test;

import java.util.Calendar;

import com.bank.model.Account;
import com.bank.model.AccountTransaction;
import com.bank.service.impl.AccountImpl;
import com.bank.service.impl.AccountTransactionImpl;

import junit.framework.TestCase;

/**
 * The Class TestAccountTransactionMB.
 * 
 * @author viniciuscastro
 */
public class TestAccountTransactionMB extends TestCase {

	/** The account impl. */
	private AccountImpl accountImpl;
	
	/** The account. */
	private Account account;
	
	/** The account test. */
	private Account accountTest;
	
	/** The transaction history. */
	private AccountTransaction transactionHistory;
	
	/** The index. */
	private Integer index;
	
	/** The account transaction impl. */
	private AccountTransactionImpl accountTransactionImpl;

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		accountImpl = new AccountImpl();
		account = new Account();
		accountTest = new Account();
		transactionHistory = new AccountTransaction();
		accountTransactionImpl = new AccountTransactionImpl();
		index = null;
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * Instantiates a new test account transaction mb.
	 *
	 * @param name the name
	 */
	public TestAccountTransactionMB(String name) {
		super(name);
	}

	/**
	 * Test deposit.
	 */
	public final void testDeposit() {
		account.setId(1L);
		account = accountImpl.findAccount(account);
		account.setBalance(account.getBalance() + 1000.00F);
		transactionHistory.setTransactionType("Deposit");
		transactionHistory.setChangedDate(Calendar.getInstance().getTime());
		transactionHistory.setValue(1000.00f);
		transactionHistory.setBalance(account.getBalance());
		transactionHistory.setAccount_id(account);
		account.getTransactionHistory().add(transactionHistory);
		accountTransactionImpl.deposit(account);
		
		accountTest.setId(1L);
		accountTest = accountImpl.findAccount(account);
		
		assertEquals(account.getUserName(), accountTest.getUserName());
		assertEquals(account.getBalance(), accountTest.getBalance());
		assertEquals(account.getId(), accountTest.getId());
		if (account.getTransactionHistory() != null) {
			index = account.getTransactionHistory().size() - 1;
			if (index != null && index >= 0) {
				assertEquals(account.getTransactionHistory().get(index).getTransactionType(), accountTest.getTransactionHistory().get(index).getTransactionType());
				assertEquals(account.getTransactionHistory().get(index).getAccount_id().getId(), accountTest.getTransactionHistory().get(index).getAccount_id().getId());
				assertEquals(account.getTransactionHistory().get(index).getBalance(), accountTest.getTransactionHistory().get(index).getBalance());
				assertEquals(account.getTransactionHistory().get(index).getId(), accountTest.getTransactionHistory().get(index).getId());
				assertEquals(account.getTransactionHistory().get(index).getValue(), accountTest.getTransactionHistory().get(index).getValue());
			}
			
		}
	}

	/**
	 * Test withdraw.
	 */
	public final void testWithdraw() {
		account.setId(1L);
		account = accountImpl.findAccount(account);
		account.setBalance(account.getBalance() - 100.00F);

		transactionHistory.setTransactionType("Withdraw");
		transactionHistory.setChangedDate(Calendar.getInstance().getTime());
		transactionHistory.setValue(100.00f);
		transactionHistory.setBalance(account.getBalance());
		transactionHistory.setAccount_id(account);
		account.getTransactionHistory().add(transactionHistory);
		accountTransactionImpl.deposit(account);

		accountTest.setId(1L);
		accountTest = accountImpl.findAccount(account);

		assertEquals(account.getUserName(), accountTest.getUserName());
		assertEquals(account.getBalance(), accountTest.getBalance());
		assertEquals(account.getId(), accountTest.getId());
		if (account.getTransactionHistory() != null) {
			index = account.getTransactionHistory().size() - 1;
			if (index != null && index >= 0) {
				assertEquals(account.getTransactionHistory().get(index).getTransactionType(), accountTest.getTransactionHistory().get(index).getTransactionType());
				assertEquals(account.getTransactionHistory().get(index).getAccount_id().getId(), accountTest.getTransactionHistory().get(index).getAccount_id().getId());
				assertEquals(account.getTransactionHistory().get(index).getBalance(), accountTest.getTransactionHistory().get(index).getBalance());
				assertEquals(account.getTransactionHistory().get(index).getId(), accountTest.getTransactionHistory().get(index).getId());
				assertEquals(account.getTransactionHistory().get(index).getValue(), accountTest.getTransactionHistory().get(index).getValue());
			}
		}
	}

	/**
	 * Test transfer.
	 */
	public final void testTransfer() {
		testTransferAccountReceiving();
		testTransferAccount();
	}
	
	/**
	 * Test transfer account receiving.
	 */
	public void testTransferAccountReceiving() {
		account.setId(1L);
		account = accountImpl.findAccount(account);
		account.setBalance(account.getBalance() + 50.00F);

		transactionHistory.setTransactionType("Transfer");
		transactionHistory.setChangedDate(Calendar.getInstance().getTime());
		transactionHistory.setValue(100.00f);
		transactionHistory.setBalance(account.getBalance());
		transactionHistory.setAccount_id(account);
		account.getTransactionHistory().add(transactionHistory);
		accountTransactionImpl.deposit(account);
		
		accountTest.setId(1L);
		accountTest = accountImpl.findAccount(account);
		
		assertEquals(account.getUserName(), accountTest.getUserName());
		assertEquals(account.getBalance(), accountTest.getBalance());
		assertEquals(account.getId(), accountTest.getId());
		if (account.getTransactionHistory() != null) {
			index = account.getTransactionHistory().size() - 1;
			if (index != null && index >= 0) {
				assertEquals(account.getTransactionHistory().get(index).getTransactionType(), accountTest.getTransactionHistory().get(index).getTransactionType());
				assertEquals(account.getTransactionHistory().get(index).getAccount_id().getId(), accountTest.getTransactionHistory().get(index).getAccount_id().getId());
				assertEquals(account.getTransactionHistory().get(index).getBalance(), accountTest.getTransactionHistory().get(index).getBalance());
				assertEquals(account.getTransactionHistory().get(index).getId(), accountTest.getTransactionHistory().get(index).getId());
				assertEquals(account.getTransactionHistory().get(index).getValue(), accountTest.getTransactionHistory().get(index).getValue());
			}
			
		}
	}

	
	/**
	 * Test transfer account.
	 */
	public void testTransferAccount() {
		account.setId(2L);
		account = accountImpl.findAccount(account);
		account.setBalance(account.getBalance() - 50.00F);

		transactionHistory.setTransactionType("Transfer");
		transactionHistory.setChangedDate(Calendar.getInstance().getTime());
		transactionHistory.setValue(100.00f);
		transactionHistory.setBalance(account.getBalance());
		transactionHistory.setAccount_id(account);
		account.getTransactionHistory().add(transactionHistory);
		accountTransactionImpl.deposit(account);

		accountTest.setId(2L);
		accountTest = accountImpl.findAccount(account);

		assertEquals(account.getUserName(), accountTest.getUserName());
		assertEquals(account.getBalance(), accountTest.getBalance());
		assertEquals(account.getId(), accountTest.getId());
		if (account.getTransactionHistory() != null) {
			index = account.getTransactionHistory().size() - 1;
			if (index != null && index >= 0) {
				assertEquals(account.getTransactionHistory().get(index).getTransactionType(), accountTest.getTransactionHistory().get(index).getTransactionType());
				assertEquals(account.getTransactionHistory().get(index).getAccount_id().getId(), accountTest.getTransactionHistory().get(index).getAccount_id().getId());
				assertEquals(account.getTransactionHistory().get(index).getBalance(), accountTest.getTransactionHistory().get(index).getBalance());
				assertEquals(account.getTransactionHistory().get(index).getId(), accountTest.getTransactionHistory().get(index).getId());
				assertEquals(account.getTransactionHistory().get(index).getValue(), accountTest.getTransactionHistory().get(index).getValue());
			}
		}
	}

}
